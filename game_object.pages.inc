<?php

/**
 * @file
 * Page callbacks for the Game Utilities: Object module.
 */

/**
 * Page callback for game/object/[oid].
 */
function game_object_view_page($game_object) {
  return t($game_object->description);
}
