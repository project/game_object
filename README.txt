
CONTENTS OF THIS FILE
---------------------

 * Introduction and Initial Design Goals
 * Installation
 * Developer's API
 * TODO

INTRODUCTION AND INITIAL DESIGN GOALS
=====================================

The Game Utilities: Object module provides a structure and API for Game Objects
on your site. It defines the GameObject class that can be used to build other
object types suitable for many games, such as Swords, Trolls, and other items.

INSTALLATION
============

1. Copy the files to your sites/SITENAME/modules directory.
   Or, alternatively, to your sites/all/modules directory.

2. Enable the Game Utilities: Object module at admin/build/modules.

3. You can configure Game Objects at admin/settings/game_object.

4. Set any required permissions at admin/user/permissions.

Developer's API
===============

The base class will take care of saving any attributes stored on the object.
To use, you need to reference any such attributes with $object->get($attribute)
and $object->set($attribute, $value), unless the class specially defines or
overrides a function for accessing them, such as $object->get_name() or
$object->set_location($value).

Please see the module code for more documentation.

TODO
====
* Create an example class or three extending the basic GameObject class.
