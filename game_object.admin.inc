<?php

/**
 * @file
 * Admin functions for the Game Utilities: Objects module.
 */

/**
 * Configure Game Objects; menu callback for admin/settings/game_object.
 */
function game_object_settings() {
  $form = array();

  $types = game_object_types();

  $items = array();

  foreach ($types as $class_name => $title) {
    $items[] = t($title);
  }

  $form['types'] = array(
    '#type' => 'item',
    '#title' => t('Available game object types'),
    '#value' => theme('item_list', $items),
  );

  return system_settings_form($form);
}
