<?php

/**
 * @file
 * Defines the game_object object.
 */

class GameObject {
  // The game object id.
  public $oid;

  // Attributes for the object. Note that these will initially be set with
  // the values returned by the ->defaults() function. To access an object's
  // attributes, you should use the various get_ATTRIBUTE and set_ATTRIBUTE
  // functions, such as $this->get_location() and $this->set_title('Alfred').
  public $location;
  public $title;
  public $description;

  // The timestamp when the object was initially created.
  public $__created;

  // The object's class name.
  public $__class_name;

  // The set of all object attributes. You should never use this variable.
  public $__attributes;

  // Set when a property in the object has been changed.
  // The object will be automatically saved on destruct in such a case.
  public $__changed;

  // Set when the game object has been deleted.
  // Ensures it won't be inadvertently rewritten to the database.
  public $__deleted;

  /**
   * Return an array of object attribute defaults.
   *
   * Any derived classes should register any defined or overridden attributes
   * here, to ensure they are stored properly in the database. If this function
   * is overridden, it must merge any attribute defaults defined by the parent.
   */
  function defaults() {
    return array(
      'location' => 0,
      'title' => '',
      'description' => '',
    );
  }

  /**
   * Returns the object OID where the object is located.
   */
  function get_location() {
    return $this->get('location');
  }

  /**
   * Low level function to set the object's location.
   *
   * In general, you should use ->move instead, which will ensure the object
   * is legally allowed to be moved into the location, and clean up contents
   * of the old location.
   */
  function set_location($value) {
    return $this->set('location', $value);
  }

  /**
   * Returns the object's title.
   *
   * This low level function will return the raw, untranslated title of the
   * object. This function is often used to create links.
   */
  function get_title() {
    return $this->get('title');
  }

  /**
   * Sets the object's title.
   */
  function set_title($value) {
    return $this->set('title', $value);
  }

  /**
   * Returns the object's raw description.
   */
  function get_description() {
    return $this->get('description');
  }

  /**
   * Sets the object's description.
   */
  function set_description($value) {
    return $this->set('description', $value);
  }

  function __construct($__attributes = array()) {
    // First set any default values.
    foreach ($this->defaults() as $key => $value) {
      $this->$key = $value;
    }

    // Now add any overrides passed in the parameters.
    if (is_array($__attributes)) {
      // Preload each of the returned __attributes.
      foreach ($__attributes as $key => $value) {
        $this->$key = $value;
      }
    }

    // Set the time of object creation if not already set.
    $this->__created = $this->_timestamp($this->__created);

    // Set the object's class name. This will ensure the object will be
    // instantiated to the proper class when loading back from the db later.
    $this->__class_name = get_class($this);

    // Automatic insertion of new objects so we can get a valid oid.
    if (empty($this->oid)) {
      $this->is_new = TRUE;
      $this->write();
    }
  }

  function __destruct() {
    if ($this->__changed && !$this->__deleted) {
      $this->write();
    }
  }

  /**
   * A low level function to get the attribute's value.
   *
   * In most circumstances, you'll want to instead use the specific function
   * for getting that attribute's value, such as ->get_title or ->get_location.
   * This allows for overrides and actions. For instance, a bystander might
   * not be able to examine the contents of another's backpack, even though
   * that person would be able to.
   *
   * @param string $attribute
   *  The attribute in question, such as 'openable' or 'description'.
   * @return mixed
   *  The attribute's value.
   */
  function get($attribute) {
    return $this->$attribute;
  }

  /**
   * A low level function to set the __attributes value.
   *
   * In most circumstances, you'll want to instead use the specific function
   * for setting that attribute's value, such as ->set_title or ->set_location.
   * This allows for overrides and actions. For instance, a bell might ring
   * every time a door is opened, so you would want to use ->set_open instead.
   *
   * Note that this function will set the object's __changed value to TRUE,
   * so that it will be saved before the page exits.
   *
   * @param string $attribute
   *  The attribute in question, such as 'closed' or 'health'.
   * @param mixed $value
   *  The value to which to set the attribute.
   * @return mixed
   *  The attribute's new value.
   */
  function set($attribute, $value) {
    if ($this->$attribute != $value) {
      $this->$attribute = $value;
      $this->__changed = TRUE;
    }
    return $this->$attribute;
  }

  /**
   * Write this object to the database.
   */
  function write() {
    // Only write the object if it hasn't been marked for deletion.
    if (!$this->__deleted) {
      // Reset the __deleted & __changed values.
      unset($this->__deleted);
      unset($this->__changed);

      unset($this->__attributes);
      $this->__attributes = (array)$this;

      // Create the array of attributes to be recorded.
      foreach ($this->defaults() as $attribute => $default) {
        // We only record the attribute if it's different than the default.
        if ($this->$attribute != $default) {
          $this->__attributes[$attribute] = $this->$attribute;
        }

        // However, we need to unset any values with defined columns.
        foreach (array('oid', '__class_name', 'location', 'title', 'description', '__created', 'is_new') as $key) {
          unset($this->__attributes[$key]);
        }
      }

      // Write the object to the database.
      if ($this->is_new) {
        unset($this->is_new);
        return drupal_write_record('game_objects', $this);
      }
      else {
        return drupal_write_record('game_objects', $this, array('oid'));
      }
    }

    // We weren't able to write to the database, because the object has already
    // been deleted.
    return FALSE;
  }

  /**
   * Delete the object from the database.
   */
  function delete() {
    // Mark the object as deleted so it isn't inadvertently saved back to the
    // database later.
    $this->__deleted = TRUE;

    return db_query("DELETE FROM {game_objects} WHERE oid = %d", $this->oid);
  }

  /**
   * This will return either the current time, or the timestamp if already set.
   */
  private function _timestamp($timestamp = NULL) {
    if (!isset($timestamp)) {
      $timestamp = time();
    }
    return $timestamp;
  }
}
